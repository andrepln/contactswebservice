using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Services;
using System.Data;
using Collab.OneContact.General;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
//using System.Data.SqlClient;
using System.Text;
using Collab.OneContact.General.Storage;
using Collab.Tools;
using System.Data.OracleClient;
using System.IO;

namespace Collab.OneContact.ContactsWebService
{
	/// <summary>
	/// Summary description for Service1.
	/// </summary>
	public class ContactsWebService : System.Web.Services.WebService, IContactsWebService
	{
		public ContactsWebService()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code

		//Required by the Web Services Designer 
		private IContainer components = null;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion

		#region IContactsWebService Members

		private string ValidSqlString(string s)
		{
			StringBuilder sb = new StringBuilder("'");
			if ((s.Length == 0) || (s.IndexOf('\'') < 0))
			{
				sb.Append(s);
			}
			else
			{
				char[] cs = s.ToCharArray();
				foreach (char c in cs)
				{
					if (c == '\'')
					{
						sb.Append("''");
					}
					else
					{
						sb.Append(c);
					}
				}
			}
			sb.Append("'");
			return sb.ToString();
		}

		private bool UseContactsWebServiceTables
		{
			get
			{
				var sps = System.Configuration.ConfigurationManager.AppSettings["UseContactsWebServiceTables"];
				if (!String.IsNullOrEmpty(sps) && sps.ToLower().Contains("false"))
					return false;

				return true;
			}
		}

		private string SqlConnectionString
		{
			get
			{
				ConnectionStringSettings conn = ConfigurationManager.ConnectionStrings["ContactsWebServiceDBConnection"];
				if (conn != null)
				{
					return AESHelper.DecryptConnectionString(conn.ConnectionString);
				}

				throw new Exception("ContactsWebServiceDBConnection ConnectionString is not encrypted. Please use the ConnectionStringWriter to encrypt it.");
			}
		}

		[WebMethod]
		public TimeZones GetTimezones(int serviceID)
		{
			throw new NotImplementedException("This Service is not Implemented, its only for inteface harmony");

			//if (!UseContactsWebServiceTables)
			//{
			//    var timezones = TimeZoneInfo.GetSystemTimeZones();
			//    //returns a DataModel will all of system Timezones
			//    return TimeZones.CreateDataModel(timezones); ;
			//}

			//var dataModel = new TimeZones();
			//SqlHelper.FillDataset(SqlConnectionString, "dbo.spSDKOutboundGetTimeZones", dataModel, new string[] { "TimeZones" }, serviceID);

			//return dataModel;
		}



		[WebMethod]
		public bool NotifyContactState(NotificationList notificationList)
		{
            //if (!UseContactsWebServiceTables) return true;
            //var queryBatch = new StringBuilder();

            //for (int i = 0; i < notificationList.Notification.Count; i++)
            //{
            //    var notificationRow = notificationList.Notification[i];
            //    queryBatch.Append("exec " + AliasProvisioning.spSDKOutboundUpdateContactState + " ")
            //    .Append(notificationRow.ContactID + ",").
            //    Append(notificationRow.ServiceID + ",").
            //    Append(notificationRow.ListID + ",").
            //    Append(((ContactStateNotification)notificationRow.Notification == ContactStateNotification.Unloaded ? "null" : (object)notificationRow.Notification) + ",").
            //    Append(((ContactStateNotification)notificationRow.Notification == ContactStateNotification.Unloaded ? (object)"null" : (object)((ContactStateNotification)notificationRow.Notification).ToString()) + ",").
            //    Append(notificationRow.IsBusinessOutcomeNull() ? (object)"null" : ValidSqlString(notificationRow.BusinessOutcome)).Append(",").
            //    Append(notificationRow.IsAgentIDNull() ? (object)"null" : notificationRow.AgentID).Append(",").
            //    Append(notificationRow.IsNextTryNull() ? (object)"null" : string.Format("'{0}'", notificationRow.NextTry.ToString("yyyy-MM-dd HH:mm:ss"))).
            //    Append(Environment.NewLine);
            //}

            //if (queryBatch.Length > 0)
            //    if (SqlHelper.ExecuteNonQuery(SqlConnectionString, CommandType.Text, queryBatch.ToString()) == 0)
            //        return false;
            //return true;

            if (!UseContactsWebServiceTables)
				return true;

            MyLogFile.LogMessageToFile("##### NotifyContactState #####");
            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Existem {0} registros de Notifica��o...", notificationList.Notification.Count));
            int affectedRows = 0;

            try
            {
                ContactsServiceReference.GetContactInterationsRequest request = null;
                ContactsServiceReference.GetContactInterationsResponse response = null;
                var ws = new ContactsServiceReference.ContactsWebServiceSoapClient();

                for (int i = 0; i < notificationList.Notification.Count; i++)
                {
                    var notificationRow = notificationList.Notification[i];

                    var idService = notificationRow.ServiceID;
                    MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# idService: {0}", idService));
                    var idContact = notificationRow.ContactID;
                    MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# idContact: {0}", idContact));
                    //var idEmployee = (notificationRow.IsAgentIDNull() ? (object)null : notificationRow.AgentID);
                    var cdStateNotification = (ContactStateNotification)notificationRow.Notification;
                    var dsNotifyState = cdStateNotification.ToString();
                    MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# dsNotifyState: {0}", dsNotifyState));
                    var dtNextTry = "";
                    /*
                    MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# dsNotifyState: {0}", notificationRow.IsNextTryNull()));
                    var dtNextTry = notificationRow.IsNextTryNull() ? (object)"null" : string.Format("{0}", notificationRow.NextTry.ToString("yyyy-MM-dd HH:mm:ss"));
                    MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# dtNextTry: {0}", dtNextTry));
                    */

                    StringBuilder sql = new StringBuilder();
                    sql.Append(" SELECT ID_CALL_RESULT ");
                    sql.Append(" FROM ( ");
                    sql.Append("    SELECT R.ID_CALL_RESULT ");
                    sql.Append("    FROM CONTACT C, TMKT_CALL_RESULT R, TMKT_GROUP G, TMKT_ACTION A ");
                    sql.Append("    WHERE A.ID_SSG_COLLAB = :p_IdService ");
                    sql.Append("      AND C.ID_CONTACT = :p_IdContact ");
                    sql.Append("      AND C.ID_CONTACT = R.FK_ID_CONTACT ");
                    sql.Append("      AND R.FK_ID_GROUP = G.ID_GROUP ");
                    sql.Append("      AND G.FK_ID_ACTION = A.ID_ACTION ");
                    sql.Append("    ORDER BY R.DT_CREATED DESC ");
                    sql.Append(" ) ");
                    sql.Append(" WHERE ROWNUM = 1 ");

                    List<OracleParameter> parameters = new List<OracleParameter>();
                    parameters.Add(new OracleParameter("p_IdService", idService));
                    parameters.Add(new OracleParameter("p_IdContact", idContact));

                    try
                    {
                        var dr = MyOracleHelper.ExecuteReader(SqlConnectionString, CommandType.Text, sql.ToString(), parameters.ToArray());

                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                var idCallResult = dr["ID_CALL_RESULT"];
                                if (idCallResult != null && idCallResult != System.DBNull.Value)
                                {
                                    if (affectedRows == 0)
                                    {
                                        sql = new StringBuilder();
                                        sql.Append(" UPDATE TMKT_CALL_RESULT SET ");
                                        sql.Append("    FK_ID_INFERENCE = FK_ID_INFERENCE  ");
                                        sql.Append(" WHERE ID_CALL_RESULT = :p_IdCallResult ");

                                        parameters = new List<OracleParameter>();
                                        parameters.Add(new OracleParameter("p_IdCallResult", idCallResult));
                                        affectedRows += MyOracleHelper.ExecuteNonQuery(SqlConnectionString, CommandType.Text, sql.ToString(), parameters.ToArray());
                                    }

                                    request = new ContactsServiceReference.GetContactInterationsRequest();
                                    request.contactID = idContact;
                                    request.StartTime = DateTime.Now.ToString("yyyy-MM-dd");
                                    request.EndTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                    response = ws.GetContactInterations(request);

                                    var ds = response.GetContactInterationsResult.Tables[0];
                                    if (ds.Rows.Count > 0)
                                    {
                                        DataRow row = ds.Rows[ds.Rows.Count - 1];
                                        if(idService.ToString().Equals(row["ServiceID"] == null ? "" : row["ServiceID"].ToString()))
                                        {
                                            MyLogFile.LogMessageToFile("#NotifyContactState# Intera��o encontrada!");

                                            var ContactID = row["ContactID"] == null ? "" : row["ContactID"].ToString();
                                            var ServiceID = row["ServiceID"] == null ? "" : row["ServiceID"].ToString();
                                            var Service = row["Service"] == null ? "" : row["Service"].ToString();
                                            var AgentID = row["AgentID"] == null ? "" : row["AgentID"].ToString();
                                            var Agent = row["Agent"] == null ? "" : row["Agent"].ToString();
                                            var Date = row["Date"] == null ? "" : row["Date"].ToString();
                                            var BusyTime = row["BusyTime"] == null ? "" : row["BusyTime"].ToString();
                                            var Address = row["Address"] == null ? "" : row["Address"].ToString();
                                            var Direction = row["Direction"] == null ? "" : row["Direction"].ToString();
                                            var MediaOutcome = row["MediaOutcome"] == null ? "" : row["MediaOutcome"].ToString();
                                            var SessionID = row["SessionID"] == null ? "" : row["SessionID"].ToString();
                                            var SessionOutcome = row["SessionOutcome"] == null ? "" : row["SessionOutcome"].ToString();
                                            var BusinessOutcomeName = row["BusinessOutcomeName"] == null ? "" : row["BusinessOutcomeName"].ToString();

                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# ContactID: {0}", ContactID));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# ServiceID: {0}", ServiceID));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Service: {0}", Service));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# AgentID: {0}", AgentID));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Agent: {0}", Agent));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Date: {0}", Date));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# BusyTime: {0}", BusyTime));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Address: {0}", Address));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Direction: {0}", Direction));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# MediaOutcome: {0}", MediaOutcome));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# SessionID: {0}", SessionID));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# SessionOutcome: {0}", SessionOutcome));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# BusinessOutcomeName: {0}", BusinessOutcomeName));


                                            parameters = new List<OracleParameter>();
                                            parameters.Add(new OracleParameter("P_ID_CALL_RESULT", idCallResult));
                                            parameters.Add(new OracleParameter("P_DT_NEXT_TRY", dtNextTry));
                                            parameters.Add(new OracleParameter("P_DS_SSG_COLLAB_NOTIFY_STATE", dsNotifyState));
                                            parameters.Add(new OracleParameter("P_NU_SSG_COLLAB_AGENT_ID", AgentID));
                                            parameters.Add(new OracleParameter("P_NM_SSG_COLLAB_AGENT", Agent));
                                            parameters.Add(new OracleParameter("P_DT_SSG_COLLAB_DATE", Date));
                                            parameters.Add(new OracleParameter("P_QT_SSG_COLLAB_BUSYTIME", BusyTime));
                                            parameters.Add(new OracleParameter("P_DS_SSG_COLLAB_ADDRESS", Address));
                                            parameters.Add(new OracleParameter("P_DS_SSG_COLLAB_DIRECTION", Direction));
                                            parameters.Add(new OracleParameter("P_DS_SSG_COLLAB_MEDIA_OUTCOME", MediaOutcome));
                                            parameters.Add(new OracleParameter("P_NU_SSG_COLLAB_SESSION_ID", SessionID));
                                            parameters.Add(new OracleParameter("P_DS_SSG_COLLAB_SESSION_OUTCOM", SessionOutcome));
                                            parameters.Add(new OracleParameter("P_DS_SSG_COLLAB_BUSINESS_OUTCO", BusinessOutcomeName));
                                            MyOracleHelper.ExecuteNonQuery(SqlConnectionString, CommandType.StoredProcedure, "SSC_RULES.NOTIFY_CONTACT_STATE_INTERAT", parameters.ToArray());
                                        }
                                        else
                                        {
                                            MyLogFile.LogMessageToFile("#NotifyContactState# Intera��o n�o encontrada!");
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# idCallResult: {0}", idCallResult));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# dtNextTry: {0}", dtNextTry));
                                            MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# dsNotifyState: {0}", dsNotifyState));

                                            parameters = new List<OracleParameter>();
                                            parameters.Add(new OracleParameter("P_ID_CALL_RESULT", idCallResult));
                                            parameters.Add(new OracleParameter("P_DT_NEXT_TRY", dtNextTry));
                                            parameters.Add(new OracleParameter("P_DS_SSG_COLLAB_NOTIFY_STATE", dsNotifyState));
                                            MyOracleHelper.ExecuteNonQuery(SqlConnectionString, CommandType.StoredProcedure, "SSC_RULES.NOTIFY_CONTACT_STATE", parameters.ToArray());
                                        }
                                    }
                                    else
                                    {
                                        MyLogFile.LogMessageToFile("#NotifyContactState# Intera��o n�o encontrada!");
                                        MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# idCallResult: {0}", idCallResult));
                                        MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# dtNextTry: {0}", dtNextTry));
                                        MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# dsNotifyState: {0}", dsNotifyState));

                                        parameters = new List<OracleParameter>();
                                        parameters.Add(new OracleParameter("P_ID_CALL_RESULT", idCallResult));
                                        parameters.Add(new OracleParameter("P_DT_NEXT_TRY", dtNextTry));
                                        parameters.Add(new OracleParameter("P_DS_SSG_COLLAB_NOTIFY_STATE", dsNotifyState));
                                        MyOracleHelper.ExecuteNonQuery(SqlConnectionString, CommandType.StoredProcedure, "SSC_RULES.NOTIFY_CONTACT_STATE", parameters.ToArray());
                                    }
                                }
                                else
                                {
                                    MyLogFile.LogMessageToFile("#NotifyContactState# N�o foi encontrado um registro de TMKT_CALL_RESULT! --");
                                }
                            }
                        }

                        dr.Close();
                    }
                    catch (OracleException e)
                    {
                        MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Erro Oracle - Source: {0}", e.Source));
                        MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Erro Oracle - Message: {0}", e.Message));
                        MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Erro Oracle - StackTrace: {0}", e.StackTrace));
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Erro Generico - Source: {0}", e.Source));
                MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Erro Generico - Message: {0}", e.Message));
                MyLogFile.LogMessageToFile(String.Format("#NotifyContactState# Erro Generico - StackTrace: {0}", e.StackTrace));
                return false;
            }

			return (affectedRows > 0);
		}

		[WebMethod]
		public ContactList GetContacts(int serviceID, int numberOfContacts, AddressTypeRuleTimeZones addressTypes)
		{
			//var contactList = new ContactList();
			//var addressTypeParameter = AddressTypeRuleTimeZones.GetAddressTypeParameter(addressTypes);
			//if (!UseContactsWebServiceTables) return contactList;

			//var timestamp = DateTime.UtcNow.Ticks;

			//SqlHelper.FillDataset(SqlConnectionString, AliasProvisioning.spSDKOutboundGetContacts, contactList, new string[] { "Contact" }, serviceID, numberOfContacts, addressTypeParameter, timestamp);
			//SqlHelper.FillDataset(SqlConnectionString, "dbo.spSDKOutboundGetAddresses", contactList, new string[] { "Address" }, serviceID, timestamp);

			//return contactList;

            MyLogFile.LogMessageToFile("##### GetContacts #####");
            MyLogFile.LogMessageToFile(String.Format("#GetContacts# serviceID: {0}", serviceID));
            MyLogFile.LogMessageToFile(String.Format("#GetContacts# numberOfContacts: {0}", numberOfContacts));

            var contactList = new ContactList();
			StringBuilder sql = new StringBuilder();
			sql.Append(" SELECT ");
			sql.Append("    ID_CONTACT ContactID, ");
			sql.Append("    DT_CONTACT ContactDate, ");
			sql.Append("    CD_PRIORITY Priority, ");
			sql.Append("    FK_ID_EMPLOYEE AgentID, ");
			sql.Append("    NU_MAIN_PHONE Address0, ");
			sql.Append("    NU_CELL_PHONE Address1, ");
			sql.Append("    NU_HOME_PHONE Address2, ");
			sql.Append("    NU_COMERCIAL_PHONE Address3 ");
			sql.Append(" FROM ( ");
			sql.Append("    SELECT ");
			sql.Append("        COUNT(1) OVER() QT_TOTAL, ");
			sql.Append("        (TRUNC((ROW_NUMBER() OVER(ORDER BY COALESCE(R.DT_SCHEDULE, R.DT_CREATED) DESC) - 1) / :p_NumberOfContacts) + 1) NU_PAGE, ");
			sql.Append("        C.ID_CONTACT, ");
			sql.Append("        COALESCE(R.DT_SCHEDULE, R.DT_CREATED) DT_CONTACT, ");
			sql.Append("        CASE ");
			sql.Append("            WHEN R.CD_STATUS IS NULL THEN 5 ");
			sql.Append("            WHEN R.CD_STATUS = 11 THEN 4 ");
			sql.Append("            ELSE 1 ");
			sql.Append("        END CD_PRIORITY, ");
			sql.Append("        CASE ");
			sql.Append("            WHEN R.CD_STATUS = 11 AND R.FK_ID_LAST_EMPLOYEE IS NOT NULL THEN R.FK_ID_LAST_EMPLOYEE ");
			sql.Append("            ELSE NULL ");
			sql.Append("        END FK_ID_EMPLOYEE, ");
			sql.Append("        COALESCE(REGEXP_REPLACE(C.NU_CELL_PHONE, '\\D+', ''), REGEXP_REPLACE(C.NU_HOME_PHONE, '\\D+', ''), REGEXP_REPLACE(C.NU_COMERCIAL_PHONE, '\\D+', '')) NU_MAIN_PHONE, ");
			sql.Append("        REGEXP_REPLACE(C.NU_CELL_PHONE, '\\D+', '') NU_CELL_PHONE, ");
			sql.Append("        REGEXP_REPLACE(C.NU_HOME_PHONE, '\\D+', '') NU_HOME_PHONE, ");
			sql.Append("        REGEXP_REPLACE(C.NU_COMERCIAL_PHONE, '\\D+', '') NU_COMERCIAL_PHONE ");
			sql.Append("    FROM CONTACT C ");
            sql.Append("    JOIN TMKT_CALL_RESULT R ON C.ID_CONTACT = R.FK_ID_CONTACT ");
            sql.Append("    JOIN TMKT_GROUP G ON R.FK_ID_GROUP = G.ID_GROUP ");
            sql.Append("    JOIN TMKT_ACTION A ON G.FK_ID_ACTION = A.ID_ACTION ");
            sql.Append("    LEFT JOIN SSG_COLLAB_CALL_EVOLUTION_QTY$ CEQ ON R.ID_CALL_RESULT = CEQ.FK_ID_CALL_RESULT ");
            sql.Append("    WHERE A.ID_SSG_COLLAB = :p_ServiceID ");
            sql.Append("      AND ( R.CD_STATUS IS NULL OR R.CD_STATUS NOT IN (12) ) ");
            sql.Append("      AND ( R.CD_SSG_COLLAB_NOTIFY_STATE IS NULL OR R.DS_SSG_COLLAB_NOTIFY_STATE NOT IN ('Loaded', 'AlreadyLoaded') ) ");
            sql.Append("      AND ( DS_SSG_COLLAB_SESSION_OUTCOME IS NULL OR NOT( R.DS_SSG_COLLAB_SESSION_OUTCOME LIKE 'Done' AND R.DS_SSG_COLLAB_BUSINESS_OUTCOME IN ('VENDA', 'RECUSA') ) ) ");
            sql.Append("      AND ( CEQ.QT_COLLAB_SESSION_FAILED IS NULL OR CEQ.QT_COLLAB_SESSION_FAILED <= 3 ) ");
            sql.Append(" ) ");
			sql.Append(" WHERE NU_PAGE = 1 ");

			List<OracleParameter> parameters = new List<OracleParameter>();
			parameters.Add(new OracleParameter("p_NumberOfContacts", numberOfContacts));
			parameters.Add(new OracleParameter("p_ServiceID", serviceID));

			try
			{
				//DataReader
				var dr = MyOracleHelper.ExecuteReader(SqlConnectionString, CommandType.Text, sql.ToString(), parameters.ToArray());

                if (dr.HasRows)
				{
					while (dr.Read())
					{
						var contact = contactList.Contact.NewContactRow();
						contact.ContactID = Convert.ToInt32(dr["ContactID"]);
						contact.ContactDate = Convert.ToDateTime(dr["ContactDate"]);
						contact.Priority = Convert.ToInt16(dr["Priority"]);
						if (dr["AgentID"] != DBNull.Value)
						{
							contact.AgentID = Convert.ToInt32(dr["AgentID"]);
						}

						contactList.Contact.AddContactRow(contact);

						var address = contactList.Address.NewAddressRow();
						address.ContactID = Convert.ToInt32(dr["ContactID"]);
						address.AddressTypeID = 1;
						address.ContactAddress = dr["Address0"].ToString();

						contactList.Address.AddAddressRow(address);
					}
                }

                dr.Close();
                MyLogFile.LogMessageToFile(String.Format("#GetContacts# Foram retornados {0} registros...", contactList.Contact.Count));
                return contactList;
			}
			catch (OracleException e)
			{
                MyLogFile.LogMessageToFile(String.Format("#GetContacts# Erro Oracle - Source: {0}", e.Source));
                MyLogFile.LogMessageToFile(String.Format("#GetContacts# Erro Oracle - Message: {0}", e.Message));
                MyLogFile.LogMessageToFile(String.Format("#GetContacts# Erro Oracle - StackTrace: {0}", e.StackTrace));
                return contactList;
			}
		}

		[WebMethod]
		public ContactList GetContactsPaused(int serviceID, int numberOfContacts, AddressTypeRuleTimeZones addressTypes)
		{
			return this.GetContacts(serviceID, numberOfContacts, addressTypes);
		}

		[WebMethod]
		public DataSet GetContactReport(int serviceID)
		{
			throw new NotImplementedException("This Service is not Implemented, its only for inteface harmony");
			//DataSet dataset = new DataSet();
			//if (!UseContactsWebServiceTables) return dataset;

			//SqlHelper.FillDataset(SqlConnectionString, "dbo.spSDKOutboundGetContactReport", dataset, new string[] { "Contact" }, serviceID);
			//return dataset;
		}

		[WebMethod]
		public string GetBusinessDataPageURL(int serviceID)
		{
			return "about:blank";
		}

		[WebMethod]
		public DataSet GetBusinessData(int serviceID)
		{
			return null;
		}

		[WebMethod]
		public bool NotifyRequestIDResult(int requestID, int numberOfContacts, string ex)
		{
			throw new NotImplementedException("This Service is not Implemented, its only for inteface harmony");
			//if (!UseContactsWebServiceTables) return true;

			//if (SqlHelper.ExecuteNonQuery(SqlConnectionString,
			//    "dbo.spSDKOutboundRequestIDResult",
			//    requestID,
			//    numberOfContacts,
			//    null == ex ? System.DBNull.Value : (object)ex) == 0)
			//    return false;
			//else
			//    return true;
		}

		#endregion

		#region Aux

		/// <summary>
		/// This function inserts a contact with one address directly into the contact list
		/// </summary>
		/// <param name="list">The contact list in which to insert the contact</param>
		/// <param name="contactID">The ID of the contact</param>
		/// <param name="serviceID">The service ID of the contact</param>
		/// <param name="contactDate">The date on which the contact should be executed</param>
		/// <param name="priority">The priority of the contact</param>
		/// <param name="agentID">The ID of the preferred agent</param>
		/// <param name="address">The address of the contact</param>
		/// <param name="addressTypeID">The ID if the address type</param>
		void InsertIntoList(ContactList list,
							int contactID,
							int listID,
							int serviceID,
							System.DateTime contactDate,
							short priority,
							int agentID,
							string address,
							int addressTypeID,
							string timeZone = "")
		{
			ContactList.ContactRow contactRow = list.Contact.AddContactRow(contactID, contactDate, priority, agentID, timeZone, listID);
			list.Address.AddAddressRow(address, addressTypeID, contactRow, contactRow);
		}

		/// <summary>
		/// This function inserts an extra address into the contact list for an already inserted contact
		/// </summary>
		/// <param name="list">The contact list in which to insert the address</param>
		/// <param name="contactID">The ID of the contact</param>
		/// <param name="address">The address of the contact</param>
		/// <param name="addressTypeID">The ID of the address type</param>
		void InsertIntoList(ContactList list,
							int contactID,
							string address,
							int addressTypeID)
		{
			list.Address.AddAddressRow(address, addressTypeID, list.Contact.FindByContactID(contactID), list.Contact.FindByContactID(contactID));
		}

		#endregion
	}

	public sealed class MyOracleHelper
	{
		#region OracleHelper
		private enum OracleConnectionOwnership
		{
			/// <summary>Connection is owned and managed by SqlHelper</summary>
			Internal,
			/// <summary>Connection is owned and managed by the caller</summary>
			External
		}
		public static OracleDataReader ExecuteReader(string connectionString, CommandType commandType, string commandText, params OracleParameter[] commandParameters)
		{
			//create & open a OracleConnection
			OracleConnection cn = new OracleConnection(connectionString);
			cn.Open();

			try
			{
				//call the private overload that takes an internally owned connection in place of the connection string
				return ExecuteReader(cn, null, commandType, commandText, commandParameters, OracleConnectionOwnership.Internal);
			}
			catch
			{
				//if we fail to return the SqlDatReader, we need to close the connection ourselves
				cn.Close();
				throw;
			}
		}
		private static OracleDataReader ExecuteReader(OracleConnection connection, OracleTransaction transaction, CommandType commandType, string commandText, OracleParameter[] commandParameters, OracleConnectionOwnership connectionOwnership)
		{
			//create a command and prepare it for execution
			OracleCommand cmd = new OracleCommand();
			PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters);

			//create a reader
			OracleDataReader dr;

			// call ExecuteReader with the appropriate CommandBehavior
			if (connectionOwnership == OracleConnectionOwnership.External)
			{
				dr = cmd.ExecuteReader();
			}
			else
			{
				dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
			}

			// detach the OracleParameters from the command object, so they can be used again.
			cmd.Parameters.Clear();

			return dr;
		}

		public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText, params OracleParameter[] commandParameters)
		{
			//create & open a OracleConnection, and dispose of it after we are done.
			using (OracleConnection cn = new OracleConnection(connectionString))
			{
				cn.Open();

				//call the overload that takes a connection in place of the connection string
				return ExecuteNonQuery(cn, commandType, commandText, commandParameters);
			}
		}
		public static int ExecuteNonQuery(OracleConnection connection, CommandType commandType, string commandText, params OracleParameter[] commandParameters)
		{
			//create a command and prepare it for execution
			OracleCommand cmd = new OracleCommand();
			PrepareCommand(cmd, connection, (OracleTransaction)null, commandType, commandText, commandParameters);

			//finally, execute the command.
			int retval = cmd.ExecuteNonQuery();

			// detach the OracleParameters from the command object, so they can be used again.
			cmd.Parameters.Clear();
            connection.Close();

            return retval;
		}
		private static void PrepareCommand(OracleCommand command, OracleConnection connection, OracleTransaction transaction, CommandType commandType, string commandText, OracleParameter[] commandParameters)
		{
			//if the provided connection is not open, we will open it
			if (connection.State != ConnectionState.Open)
			{
				connection.Open();
			}

			//associate the connection with the command
			command.Connection = connection;

			//set the command text (stored procedure name or SQL statement)
			command.CommandText = commandText;

			//if we were provided a transaction, assign it.
			if (transaction != null)
			{
				command.Transaction = transaction;
			}

			//set the command type
			command.CommandType = commandType;

			//attach the command parameters if they are provided
			if (commandParameters != null)
			{
				AttachParameters(command, commandParameters);
			}

			return;
		}
		private static void AttachParameters(OracleCommand command, OracleParameter[] commandParameters)
		{
			foreach (OracleParameter p in commandParameters)
			{
				//check for derived output value with no value assigned
				if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
				{
					p.Value = DBNull.Value;
				}

				command.Parameters.Add(p);
			}
		}
		#endregion
	}

    public sealed class Test
    {
        public void SomeMethod()
        {
            ContactsServiceReference.GetContactInterationsRequest request = new ContactsServiceReference.GetContactInterationsRequest();
			request.contactID = 1823576;
			request.StartTime = DateTime.Parse("").ToString();
			request.EndTime = DateTime.Parse("").ToString();

            ContactsServiceReference.GetContactInterationsResponse response = null;

			var ws = new ContactsServiceReference.ContactsWebServiceSoapClient();
            response = ws.GetContactInterations(request);

            var ds = response.GetContactInterationsResult.Tables[0];
			foreach (DataRow row in ds.Rows)
			{
				var ContactID = row["ContactID"];
				var ServiceID = row["ServiceID"];
				var Service = row["Service"];
				var AgentID = row["AgentID"];
				var Agent = row["Agent"];
				var Date = row["Date"];
				var Address = row["Address"];
				var MediaOutcome = row["MediaOutcome"];
				var SessionID = row["SessionID"];
				var Direction = row["Direction"];
				var SessionOutcome = row["SessionOutcome"];
				var BusinessOutcomeName = row["BusinessOutcomeName"];
				var BusyTime = row["BusyTime"];
			}
        }
    }

	public sealed class MyLogFile
	{
        #region LogFile
        //private static readonly string LOG_FILENAME = Directory.GetCurrentDirectory() + "\\logs-cws\\" + ("CWS-LogFile-" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt");
        private static readonly string LOG_FILENAME = (System.Environment.GetEnvironmentVariable("CWS_LOG_PATH", EnvironmentVariableTarget.Machine) + "CWS-LogFile-" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt");

        public static void LogMessageToFile(string msg)
		{
            msg = string.Format("{0:G}: {1}{2}", DateTime.Now, msg, Environment.NewLine);
			File.AppendAllText(LOG_FILENAME, msg);
		}
		#endregion
	}

}